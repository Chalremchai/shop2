from django.db import models

# Create your models here.
class Shop(models.Model):
    shop_name = models.CharField(max_length=50)
    descriptions = models.TextField()
    catagory = models.TextField()

class Product(models.Model):
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=50)
    unit_price = models.CharField(max_length=50)
    unit_of_measure = models.CharField(max_length=50)
    product_catagory = models.TextField()
    product_image = models.ImageField()
    product_description = models.TextField()

class OrderProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    total = models.FloatField()

class Order(models.Model):
    order_product = models.ForeignKey(OrderProduct, on_delete=models.CASCADE)
    unit_amount = models.CharField(max_length=50)
    payment_method = models.CharField(max_length=50)
    total_price = models.DecimalField( max_digits=9, decimal_places=2)