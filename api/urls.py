from django.urls import include,path
from rest_framework import routers
from api import views
router = routers.DefaultRouter()

router.register(r'shop', views.ShopViewSet)
router.register(r'product', views.ProductViewSet)
router.register(r'orderproduct', views.OrderProductViewSet)
router.register(r'order', views.OrderViewSet)

urlpatterns = [
    path('' ,include(router.urls)),
]
