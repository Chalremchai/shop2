from .models import Shop,Product,OrderProduct,Order
from rest_framework import serializers

class ShopSerializer(serializers.ModelSerializer):
    class Meta :
        model = Shop
        fields = ("__all__")

class ProductSerializer(serializers.ModelSerializer):
    shop = ShopSerializer()
    class Meta :
        model = Product
        fields = ("__all__")
class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta :
        model = Product
        fields = ("__all__")

class OrderProductSerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    class Meta :
        model = OrderProduct
        fields = ("__all__")
class OrderProductCreateSerializer(serializers.ModelSerializer):
    class Meta :
        model = OrderProduct
        fields = ("__all__")
        
class OrderSerializer(serializers.ModelSerializer):
    order_product = OrderProductSerializer()
    class Meta :
        model = Order
        fields = ("__all__")
class OrderCreateSerializer(serializers.ModelSerializer):
    class Meta :
        model = Order
        fields = ("__all__")